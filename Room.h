#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include "Item.h"

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;

public:
    Room(const std::string& desc) : description(desc) {}

    void AddItem(const Item& item) {
        items.push_back(item);
    }

    void RemoveItem(const std::string& itemName) {
        auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& item) {
            return item.GetName() == itemName;
        });

        if (it != items.end()) {
            items.erase(it);
        }
    }

    void AddExit(const std::string& direction, Room* room) {
        exits[direction] = room;
    }

    Room* GetExit(const std::string& direction) {
        auto it = exits.find(direction);
        if (it != exits.end()) {
            return it->second;
        }
        return nullptr;
    }

    const std::vector<Item>& GetItems() const {
  return items;
}

    std::string GetDescription() const {
  return description;
}

    const std::map<std::string, Room*>& GetExits()     const {
  return exits;
}
};

#endif // ROOM_H