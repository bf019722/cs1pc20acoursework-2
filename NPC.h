#ifndef NPC_H
#define NPC_H

#include "Character.h"
#include <string>

class NPC : public Character {
private:
    std::string currentRoom;
public:
    NPC(const std::string& name, int health, const std::string& startingRoom) 
        : Character(name, health), currentRoom(startingRoom) {}

    std::string GetCurrentRoom() const {
        return currentRoom;
    }

void Defeat() {
    SetHealth(0);
}

protected:
void SetHealth(int health) {
    this->health = health;
}
};

#endif // NPC_H