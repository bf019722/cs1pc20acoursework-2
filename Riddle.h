#ifndef RIDDLE_H
#define RIDDLE_H

#include <vector>
#include <string>
#include <utility> 

class Riddle {
private:
    std::vector<std::pair<std::string, std::string>> riddles;
public:
    Riddle();
    bool loadFromFile(const std::string& filename);
    std::pair<std::string, std::string> getRandomRiddle() const;
};

#endif // RIDDLE_H
