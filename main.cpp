#include "Area.h"
#include "Item.h"
#include "Player.h"
#include "NPC.h"
#include "CommandInterpreter.h"
#include "Riddle.h"
#include <iostream>
#include <limits>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <random>

int main() {
    Area gameArea;
    gameArea.LoadMapFromFile("game_map.txt");

    Item key("Key", "A shiny key that looks important.");
    Item sword("Sword", "A sharp sword with a golden hilt.");
    Item lantern("Lantern", "An old lantern that may still work.");
    Item map("Map", "A map of the area, it looks worn but readable.");
    Item potion("Potion", "A small vial containing a red liquid that glimmers.");
    Item treasure("Treasure", "A small chest filled with gold and jewels.");

    Player player("Alice", 100);
    CommandInterpreter interpreter;
    Riddle riddleManager;

    if(!riddleManager.loadFromFile("riddles.txt")) {
        std::cerr << "Failed to load riddles." << std::endl;
        return 1;
    }

    std::vector<std::string> roomNames = {"StartRoom", "TreasureRoom"}; // Populate with actual room names
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr(0, roomNames.size() - 1);
    std::string npcStartingRoom = roomNames[distr(gen)];

    NPC npc("Bob", 100, npcStartingRoom);

    Room* startRoom = gameArea.GetRoom("StartRoom");
    if (startRoom) {
        startRoom->AddItem(lantern);
        startRoom->AddItem(map);
        player.SetLocation(startRoom);
    } else {
        std::cerr << "Starting room does not exist in the map." << std::endl;
        return 1;
    }

    Room* treasureRoom = gameArea.GetRoom("TreasureRoom");
    if (treasureRoom) {
        treasureRoom->AddItem(sword);
        treasureRoom->AddItem(treasure);
    }

    bool gameRunning = true;
    while (gameRunning) {
        Room* currentRoom = player.GetLocation();

        std::cout << "\nCurrent Location: " << currentRoom->GetDescription() << std::endl;
        std::cout << "Items in the room:" << std::endl;
        for (const Item& item : currentRoom->GetItems()) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << "\n" << std::endl;
        }

        std::cout << "Exits available: ";
        for (const auto& exit : currentRoom->GetExits()) {
            std::cout << exit.first << " ";
        }
        std::cout << std::endl;

        std::cout << "Options: " << std::endl;
        std::cout << "1. Look around " << std::endl;
        std::cout << "2. Pick up an item " << std::endl;
        std::cout << "3. Interact with an item in your inventory " << std::endl;
        std::cout << "4. Move to another room " << std::endl;
        std::cout << "5. Quit " << std::endl;

        std::cout << "Enter your choice: ";
        std::string commandLine;
        std::getline(std::cin, commandLine);

        interpreter.InterpretCommand(commandLine, player);

        if (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Invalid input. Please enter a command." << std::endl;
        }

        if(currentRoom == gameArea.GetRoom(npc.GetCurrentRoom())) {
            auto riddle = riddleManager.getRandomRiddle();
            std::cout << "You encounter Bob! Solve this riddle or face defeat:" << std::endl;
            std::cout << riddle.first << std::endl;

            std::string answer;
            std::getline(std::cin, answer);
            if(answer == riddle.second) {
                std::cout << "Correct! Bob has been defeated." << std::endl;
                npc.Defeat();
            } else {
                std::cout << "Incorrect! Bob defeats you." << std::endl;
                gameRunning = false;
            }
        }

        if (!gameRunning) {
            std::cout << "Game over." << std::endl;
            break;
        }
    }

    return 0;
}
