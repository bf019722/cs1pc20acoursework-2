#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include <string>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}

    void Interact() const {
        std::cout << "You examine the " << name << ": " << description << std::endl;
    }

    std::string GetName() const { return name; }
    std::string GetDescription() const { return description; }
};

#endif // ITEM_H
