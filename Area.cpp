#include "Area.h"
#include <vector>
#include <iterator>

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) const {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        return it->second;
    }
    return nullptr;
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);
    if (room1 && room2) {
        room1->AddExit(direction, room2);
    }
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Could not open the file - '" << filename << "'" << std::endl;
        return;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::vector<std::string> tokens;
        std::string token;
        while (std::getline(iss, token, '|')) {
            tokens.push_back(token);
        }

        if (tokens.size() == 2) {
            // This line is defining a room.
            AddRoom(tokens[0], new Room(tokens[1]));
        } else if (tokens.size() == 3) {
            // This line is defining a connection.
            ConnectRooms(tokens[0], tokens[1], tokens[2]);
        } else {
            std::cerr << "Invalid line format in game map file: " << line << std::endl;
        }
    }

    file.close();
}
