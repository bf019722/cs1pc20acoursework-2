#ifndef COMMANDINTERPRETER_H
#define COMMANDINTERPRETER_H

#include "Character.h"
#include "Player.h"
#include <string>
#include <unordered_map>
#include <functional>
#include <iostream>

class CommandInterpreter {
private:
    std::unordered_map<std::string, std::function<void(Character&, const std::string&)>> commandMap;

public:
    CommandInterpreter() {
        commandMap["look"] = [](Character& character, const std::string&) {
            if (auto* player = dynamic_cast<Player*>(&character)) {
                player->LookAround();
            }
        };
        commandMap["move"] = [](Character& character, const std::string& args) {
            if (auto* player = dynamic_cast<Player*>(&character)) {
                player->Move(args);
            }
        };
        commandMap["pick"] = [](Character& character, const std::string& args) {
            if (auto* player = dynamic_cast<Player*>(&character)) {
                player->PickUpItem(args);
            }
        };
        commandMap["interact"] = [](Character& character, const std::string& args) {
            if (auto* player = dynamic_cast<Player*>(&character)) {
                player->InteractWithItem(args);
            }
        };
    }

    void InterpretCommand(const std::string& input, Character& character) {
        auto argsBegin = input.find(' ');
        std::string commandWord = input.substr(0, argsBegin);
        std::string args = (argsBegin != std::string::npos) ? input.substr(argsBegin + 1) : "";

        auto action = commandMap.find(commandWord);
        if (action != commandMap.end()) {
            action->second(character, args);
        } else {
            std::cout << "Unknown command." << std::endl;
        }
    }
};

#endif // COMMANDINTERPRETER_H
