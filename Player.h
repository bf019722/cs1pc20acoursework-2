#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include "Room.h"
#include <iostream>
#include <string>

class Player : public Character {
private:
    Room* location;

public:
    Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

    void SetLocation(Room* newLocation) {
        location = newLocation;
    }

    Room* GetLocation() const {
        return location;
    }

    void LookAround() const {
        std::cout << "Looking around: " << location->GetDescription() << std::endl;
    }

    void Move(const std::string& direction) {
        Room* nextRoom = location->GetExit(direction);
        if (nextRoom) {
            SetLocation(nextRoom);
            std::cout << "You move to the " << direction << "." << std::endl;
        } else {
            std::cout << "You can't go that way." << std::endl;
        }
    }

    void PickUpItem(const std::string& itemName) {
        auto& items = location->GetItems();
        auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& item) {
            return item.GetName() == itemName;
        });
        if (it != items.end()) {
            AddItemToInventory(*it);
            location->RemoveItem(itemName);
            std::cout << "Picked up " << itemName << "." << std::endl;
        } else {
            std::cout << "No such item in the room." << std::endl;
        }
    }

    void InteractWithItem(const std::string& itemName) {
        auto& inventory = GetInventory();
        auto it = std::find_if(inventory.begin(), inventory.end(), [&itemName](const Item& item) {
            return item.GetName() == itemName;
        });
        if (it != inventory.end()) {
            it->Interact();
        } else {
            std::cout << "You don't have a " << itemName << "." << std::endl;
        }
    }
};

#endif // PLAYER_H
