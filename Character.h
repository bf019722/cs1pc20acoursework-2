#ifndef CHARACTER_H
#define CHARACTER_H

#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

class Character {
private:
    std::string name;
    std::vector<Item> inventory;
public:
    int health;

protected:
    std::vector<Item>& GetInventory() { return inventory; }

public:
    Character(const std::string& name, int health) : name(name), health(health) {}

    virtual ~Character() {}

    void TakeDamage(int damage) {
        health -= damage;
        if (health <= 0) {
            health = 0;
            std::cout << name << " has been defeated." << std::endl;
        }
    }

    void AddItemToInventory(const Item& item) {
        inventory.push_back(item);
    }

    std::string GetName() const { return name; }
    int GetHealth() const { return health; }
};

#endif // CHARACTER_H
