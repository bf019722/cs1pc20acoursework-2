#include "Riddle.h"
#include <fstream>
#include <sstream>
#include <random>
#include <ctime>

Riddle::Riddle() {
    std::srand(static_cast<unsigned int>(std::time(nullptr))); 
}

bool Riddle::loadFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) return false;

    std::string line;
    while (getline(file, line)) {
        std::istringstream iss(line);
        std::string question, answer;
        if (std::getline(iss, question, '|') && std::getline(iss, answer)) {
            riddles.emplace_back(question, answer);
        }
    }
    file.close();
    return true;
}

std::pair<std::string, std::string> Riddle::getRandomRiddle() const {
    if (riddles.empty()) return {"", ""}; 

    size_t index = std::rand() % riddles.size();
    return riddles[index];
}
